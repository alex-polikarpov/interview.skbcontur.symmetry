﻿using System;

namespace Interview.SkbContur.Symmetry.RdpVariant2
{
    public class MirrorPair
    {
        public static int? GetXSymmetry(Point a, Point b)
        {
            if (a == null || b == null)
                throw new Exception("");

            var min = a;
            var max = b;

            if (a.X > b.X)
            {
                min = b;
                max = a;
            }

            var remotenes = max.X - min.X;
            if (remotenes % 2 == 0)
                return min.X + remotenes / 2;

            return null;
        }
    }
}
