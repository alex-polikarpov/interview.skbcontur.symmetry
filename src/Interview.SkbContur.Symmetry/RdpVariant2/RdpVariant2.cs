﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Interview.SkbContur.Symmetry.RdpVariant2
{
    /// <summary>
    /// Второй вариант. Узнал что подразумевает задание, до решения по рдп минут 15 посидел прикинул как можно решить
    /// СПОЙЛЕР - оказалось, что в тестах в задаче не все кейсы были предусмотрены и не проверяли разный порядок указания
    /// точек в массиве И не проверяли кейс когда есть несколько зеркальных точек находящихся на одной горизонтальной линии
    /// </summary>
    public class RdpVariant2
    {
        public static int? Execute(params Point[] points)
        {
            if (points == null || points.Length == 0)
                return null;

            if (points.Length == 1)
                return points[0].X;

            int? result = null;
            var splitedPoints = SplitPoints(points);
            foreach (var pair in splitedPoints.MirrorPairs)
            {
                var symmetry = MirrorPair.GetXSymmetry(pair.Item1, pair.Item2);
                if (symmetry == null)
                    return null;

                if (result == null)
                {
                    result = symmetry;
                    continue;
                }

                if (result.Value != symmetry.Value)
                    return null;
            }


            if (splitedPoints.NotPairsPoints.Count > 0)
            {
                if (result == null)
                    result = splitedPoints.NotPairsPoints[0].X;

                if (splitedPoints.NotPairsPoints.All(p => p.X == result))
                    return result;

                return null;
            }

            return result;
        }

        private static SplitPointsResult SplitPoints(Point[] points)
        {
            var skipPoints = new Dictionary<Point, Point>(points.Length / 2);
            var pairs = new List<Tuple<Point, Point>>(points.Length / 2);
            var separatedPoints = new List<Point>(points.Length / 2);

            for (int i = 0; i < points.Length; i++)
            {
                if (points[i] == null || skipPoints.ContainsKey(points[i]))
                    continue;

                skipPoints.Add(points[i], points[i]);

                var pair = points.FirstOrDefault(x => !skipPoints.ContainsKey(x) && x.Y == points[i].Y);
                if (pair == null)
                {
                    separatedPoints.Add(points[i]);
                    continue;
                }

                pairs.Add(new Tuple<Point, Point>(points[i], pair));
                skipPoints.Add(pair, pair);
            }

            var result = new SplitPointsResult()
            {
                NotPairsPoints = separatedPoints,
                MirrorPairs = pairs
            };

            return result;
        }
    }
}
