﻿using System;
using System.Linq;

namespace Interview.SkbContur.Symmetry.RpdVariant1
{
    /// <summary>
    /// Первый вариант решения задачи по rdp, когда не понял суть самой задачи
    /// и просто пытался по тестам хотя бы какие-то кейсы проверить
    /// </summary>
    public class RdpVariant1
    {
        public static int? Execute(params Point[] points)
        {
            if (points == null || points.Length < 1)
                return null;

            if (points.Length == 1)
                return points[0].X;

            var x = points[0].X;
            var y = points[0].Y;

            var pointsAnalyzer = new PointsAnalyzer(points);
            if ((pointsAnalyzer.IsStraightByX && !(Math.Abs(pointsAnalyzer.GravityCenterX % 1F) > 0F)) 
                || (pointsAnalyzer.IsStraightByY && !(Math.Abs(pointsAnalyzer.GravityCenterY % 1F) > 0F)))
            {
                var grPoint = points.FirstOrDefault(p => p.X == (int)pointsAnalyzer.GravityCenterX);
                if (grPoint != null)
                    return grPoint.X;
            }

            return null;
        }

        private class PointsAnalyzer
        {
            /// <summary>
            /// ВНИМАНИЕ - вычисление центра масс было неправильно реализованою Для ситуации -8, 4 даст неправильный результат
            /// Нужно было искать удаление двух точек друг от друга
            /// </summary>
            public PointsAnalyzer(Point[] points)
            {
                var x = points[0].X;
                var y = points[0].Y;
                IsStraightByX = true;
                IsStraightByY = true;

                var xSum = 0;
                var ySum = 0;

                foreach (var point in points)
                {
                    if (IsStraightByX && point.X != x)
                        IsStraightByX = false;

                    if (IsStraightByY && point.Y != y)
                        IsStraightByY = false;

                    xSum += point.X;
                    ySum += point.Y;
                }

                GravityCenterX = (float)xSum / (float)points.Length;
                GravityCenterY = (float)ySum / (float)points.Length;
            }

            public bool IsStraightByX { get; private set; }

            public bool IsStraightByY { get; private set; }

            public float GravityCenterX { get; private set; }

            public float GravityCenterY { get; private set; }
        }
    }
}
