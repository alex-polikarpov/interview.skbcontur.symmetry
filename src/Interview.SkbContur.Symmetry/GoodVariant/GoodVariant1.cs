﻿using System.Collections.Generic;
using System.Linq;

namespace Interview.SkbContur.Symmetry.GoodVariant
{
    /// <summary>
    /// Первый хороший вариант решения.
    /// Минусы: Выделение доп. памяти O(N)
    /// Плюсы: Время работы O(N).
    /// Можно убрать один проход если на этапе разбивки сразу вычислять сумму / среднее и хранить ее во вспомогательной переменной, но
    /// будет еще больше использование доп. памяти.
    /// </summary>
    public class GoodVariant1
    {
        public static int? Execute(Point[] points)
        {
            int? result = null;
            var dictionary = SplitPoints(points);

            foreach (var hashSet in dictionary.Values)
            {
                //у всех одиночных точек должен совпадать X с симметрией
                if (hashSet.Count < 2)
                {
                    var firstX = hashSet.First();
                    if (!result.HasValue)
                        result = firstX;

                    if (result.Value != firstX)
                        return null;

                    continue;
                }
                
                //целочисленное деление здесь допустимо. С Double еще нужно дополнительно проверять нацело ли поделили
                var average = hashSet.Sum(x => x) / hashSet.Count;

                //result содержит предположительную точку симметрии
                if (result.HasValue && result.Value != average)
                    return null;

                var xSymmetry = GetXSymmetry(hashSet, average);

                if (result == null)
                    result = xSymmetry;

                if (result == null || result != xSymmetry)
                    return null;                
            }

            return result;
        }

        /// <summary>
        /// Группирует точки по Y. Ключ Y значение массив координат по X
        /// </summary>
        private static IDictionary<int, HashSet<int>> SplitPoints(Point[] points)
        {
            var result = new Dictionary<int, HashSet<int>>();

            foreach (var point in points)
            {
                HashSet<int> hashSet = null;

                if (!result.ContainsKey(point.Y))
                {
                    hashSet = new HashSet<int>();
                    result.Add(point.Y, hashSet);
                }
                else hashSet = result[point.Y];

                hashSet.Add(point.X);
            }            

            return result;
        }

        public static int? GetXSymmetry(HashSet<int> points, int average)
        {
            foreach (var pointX in points)
            {
                if(pointX == average)
                    continue;

                var pair = ((pointX - average) * -1) + average;
                if (!points.Contains(pair))
                    return null;
            }

            return average;
        }
    }
}

